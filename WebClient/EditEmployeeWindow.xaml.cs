﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WebClient
{
    /// <summary>
    /// Interaction logic for EditEmployeeWindow.xaml
    /// </summary>
    public partial class EditEmployeeWindow : Window
    {
        public event EventHandler<Employee> AddEmployeeHandler = delegate { };
        public EditEmployeeWindow(ObservableCollection<Department> departments)
        {
            InitializeComponent();
            DepartmentsList.ItemsSource = departments;
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            AddEmployeeHandler.Invoke(sender, new Employee
            {
                Name = "Tommy",
                //Name = NameBox.Text,
                Age = 32,
                //Age = Int32.Parse(AgeBox.Text),
                Salary = 2339999
                //Salary = Int32.Parse(SalaryBox.Text)
            });
            Close();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
