﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WebClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
        private ObservableCollection<Department> departments = new ObservableCollection<Department>();
        private HttpClient client = new HttpClient();
        public MainWindow()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://localhost:50954");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
        }
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IEnumerable<Employee> employeesIE = await GetEmployeesAsync(client.BaseAddress + "api/Employees");
            if (employeesIE != null)
            {
                employees = new ObservableCollection<Employee>(employeesIE);
                employeesDataGrid.ItemsSource = employees;
            }
            IEnumerable<Department> departmentsIE = await GetDepartmentsAsync(client.BaseAddress + "api/Departments");
            if (departmentsIE != null)
            {
                departments = new ObservableCollection<Department>(departmentsIE);
                departmentsDataGrid.ItemsSource = departments;
            }
        }
        private void EmployeeWindow_Show(object sender, RoutedEventArgs e)
        {
            EditEmployeeWindow editEmployeeWindow = new EditEmployeeWindow(departments);
            editEmployeeWindow.AddEmployeeHandler += async (s, emp) =>
            {
                bool isEmployeeAdded = await PostEmployeesAsync(client.BaseAddress + "api/Employees", emp);
                if (isEmployeeAdded)
                {
                    employeesDataGrid.ItemsSource = await GetEmployeesAsync(client.BaseAddress + "api/Employees");
                }
            };
            editEmployeeWindow.ShowDialog();
        }
        private void DepartmentWindow_Show(object sender, RoutedEventArgs e)
        { }
        private async Task<bool> PostEmployeesAsync(string path, Employee emp)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(path, emp);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            return false;
        }
        /*private async void IdEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            List<Employee> employees = new List<Employee>();
            if (idEmployeeTextBox.Text != String.Empty)
            {
                Employee employee = await GetEmployeeAsync(client.BaseAddress + "api/Employees/" + idEmployeeTextBox.Text);
                if (employee != null)
                    employees.Add(employee);
            }
            else
            {
                employees = (List<Employee>)await GetEmployeesAsync(client.BaseAddress + "api/Employees");
            }
            employeesDataGrid.ItemsSource = employees;
        }*/
        private async Task<IEnumerable<Employee>> GetEmployeesAsync(string path)
        {
            IEnumerable<Employee> employees = null;
            try
            {
                HttpResponseMessage response = await client.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    employees = await response.Content.ReadAsAsync<IEnumerable<Employee>>();
                }
            }
            catch (Exception)
            {
            }

            return employees;
        }
        private async Task<IEnumerable<Department>> GetDepartmentsAsync(string path)
        {
            IEnumerable<Department> departments = null;
            try
            {
                HttpResponseMessage response = await client.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    departments = await response.Content.ReadAsAsync<IEnumerable<Department>>();
                }
            }
            catch (Exception)
            {
            }

            return departments;
        }
        private async Task<Employee> GetEmployeeAsync(string path)
        {
            Employee employee = null;
            try
            {
                HttpResponseMessage response = await client.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    employee = await response.Content.ReadAsAsync<Employee>();
                }
            }
            catch (Exception)
            {
            }
            return employee;
        }

    }
}
